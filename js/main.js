window.onload = init;

function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1489516.139717879, 5240300.399571078],
            zoom: 11,
            maxZoom: 15,
            // minZoom: 5,
            rotation: 0
        }),
        target: 'mia-mappa'
    })

    const openStreetMapStandard = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true,
        title: 'OSMStandard'
    })

    const stamenToner = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenToner'
    })

    
    const stamenWatercolor = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.'
        }),
        visible: false,
        title: 'StamenWatercolor'
    })
    
    const stamenTerrain = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenTerrain'
    })
    

    //// GRUPPO LEGENDA MAPPE
    const gruppoLayerMappeBase = new ol.layer.Group({
        layers: [
            openStreetMapStandard,
            stamenToner,
            stamenWatercolor,
            stamenTerrain,
        ]
    }) 

    map.addLayer(gruppoLayerMappeBase)

    
    
    

    //--------------------------- SWITCHER LEGENDA MAPPE ---------------------------

    const radioSwitcher = document.querySelectorAll('.selettore-ol')
    
    for(let radioInput of radioSwitcher){
        radioInput.addEventListener('change', function(){
            let mappaSelezionata = this.value

            gruppoLayerMappeBase.getLayers().forEach(element => {
                let titoloLivello = element.get('title')

                // if(titoloLivello === mappaSelezionata){
                //     element.setVisible(true)
                // }
                // else{
                //     element.setVisible(false)
                // }

                element.setVisible(titoloLivello === mappaSelezionata)
            });


        })
    }

    

    // ------------------------ LAYER ------------------------------

    const fillStyle = new ol.style.Fill({
        color: [50, 89, 255, 0.2]
    })

    const strokeStyle = new ol.style.Stroke({
        color: [20, 20, 20, 0.8],
        width: 1.5
    })

    
    const circleStyle = new ol.style.Circle({
        fill: new ol.style.Fill({
            color: [255, 255, 0, 0.7]
        }),
        radius: 7,
        stroke: strokeStyle
    })

    

    //// STROKE AMATRICE
    const strokeAmatrice = new ol.style.Stroke({
        color: [200, 20, 20, 0.8],
        width: 2
    })

    //// STROKE SANTA LUCIA
    const strokeSantalucia = new ol.style.Stroke({
        color: [20, 200, 20, 0.8],
        width: 2
    })

    //// STROKE CROGNALETO
    const strokeCrognaleto = new ol.style.Stroke({
        color: [20, 20, 200, 0.8],
        width: 2
    })


    ///
     //// CUSTOM TUTTI
    //  const map_allGeoJson = new ol.layer.VectorImage({
    //     source: map_amatriceGeoJson ({
    //         format: new ol.format.GeoJSON()
    //     }),
    //     visible: true,
    //     title: 'TuttiPercorsiValue',
    //     style: new ol.style.Style({
    //         fill: fillStyle,
    //         stroke: strokeStyle,
    //         image: circleStyle
    //     })
    // })

    // map.addLayer(map_allGeoJson)

    //// CUSTOM AMATRICE
    const map_amatriceGeoJson = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/map_amatrice.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'AmatriceValue',
        style: new ol.style.Style({
            fill: fillStyle,
            stroke: strokeAmatrice,
            image: circleStyle
        })
    })
    
    map.addLayer(map_amatriceGeoJson)

    //// CUSTOM SANTA LUCIA
    const map_santa_luciaGeoJson = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/map_santa_lucia.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'SantaluciaValue',
        style: new ol.style.Style({
            fill: fillStyle,
            stroke: strokeSantalucia,
            image: circleStyle
        })
    })
    
    map.addLayer(map_santa_luciaGeoJson)

    //// CUSTOM CROGNALETO
    const map_crognaletoGeoJson = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/map_crognaleto.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'CrognaletoValue',
        style: new ol.style.Style({
            fill: fillStyle,
            stroke: strokeCrognaleto,
            image: circleStyle
        })
    })
    
    map.addLayer(map_crognaletoGeoJson)

    //// CUSTOM TUTTI
    // const map_allGeoJson = new ol.layer.VectorImage({
    //     source: map_amatriceGeoJson ({
    //         format: new ol.format.GeoJSON()
    //     }),
    //     visible: true,
    //     title: 'TuttiPercorsiValue',
    //     style: new ol.style.Style({
    //         fill: fillStyle,
    //         stroke: strokeStyle,
    //         image: circleStyle
    //     })
    // })

    // map.addLayer(map_allGeoJson)

    //// POLIGONO SU ROMA
    const zoneRomaGeoJson = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/zoneRoma.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'personalizzato',
        style: new ol.style.Style({
            fill: fillStyle,
            stroke: strokeCrognaleto,
            image: circleStyle
        })
    })
    
    map.addLayer(zoneRomaGeoJson)

    //// CUSTOM GRUPPO LEGENDA PERCORSI
    const gruppoLayerPercorsi = new ol.layer.Group({
        layers: [
            // openStreetMapStandard,
            map_amatriceGeoJson,
            map_santa_luciaGeoJson,
            map_crognaletoGeoJson,
        ]
    }) 

    map.addLayer(gruppoLayerPercorsi)

    //--------------------------- SWITCHER LEGENDA PERCORSI ---------------------------

    const radioSwitcherdue = document.querySelectorAll('.selettore-ol-due')
    
    for(let radioInputdue of radioSwitcherdue){
        radioInputdue.addEventListener('change', function(){
            let mappaSelezionatadue = this.value

            gruppoLayerPercorsi.getLayers().forEach(element => {
                let titoloLivellodue = element.get('title')

                // if(titoloLivello === mappaSelezionata){
                //     element.setVisible(true)
                // }
                // else{
                //     element.setVisible(false)
                // }

                element.setVisible(titoloLivellodue === mappaSelezionatadue)
            });


        })
    }

    

    // ------------------------- AGGIUNTA OVERLAY LEGENDA MAPPE ------------------

    const overlayHtml = document.querySelector('.overlay-container');
    const overlayLayer = new ol.Overlay({
        element: overlayHtml
    })
    map.addOverlay(overlayLayer)

    const spanOverlayNome = document.getElementById('overlay-nome');
    const spanOverlayInfo = document.getElementById('overlay-info');

    map.on('click', function(evt){
        // console.log(evt.pixel)
        overlayLayer.setPosition(undefined)

        map.forEachFeatureAtPixel(evt.pixel, function(feature, layer){
            let nome = feature.get('Corse Ciclistiche')
            // let info = feature.get('infoAddizionali')

            spanOverlayNome.innerHTML = nome;
            // spanOverlayInfo.innerHTML = info;

            overlayLayer.setPosition(evt.coordinate)
        }
        // {
        //     layerFilter: function(layer){
        //         return layer.get('title') === "StamenTerrain"
        //     }
        // }
        )
    })
    // ------------------------- AGGIUNTA OVERLAY LEGENDA PERCORSI ------------------

    // const overlayHtml = document.querySelector('.overlay-container');
    // const overlayLayer = new ol.Overlay({
    //     element: overlayHtml
    // })
    // map.addOverlay(overlayLayer)

    // const spanOverlayNome = document.getElementById('overlay-nome');
    // const spanOverlayInfo = document.getElementById('overlay-info');

    // map.on('click', function(evt){
    //     // console.log(evt.pixel)
    //     overlayLayer.setPosition(undefined)

    //     map.forEachFeatureAtPixel(evt.pixel, function(feature, layer){
    //         let nome = feature.get('nome')
    //         let info = feature.get('infoAddizionali')

    //         spanOverlayNome.innerHTML = nome;
    //         spanOverlayInfo.innerHTML = info;

    //         overlayLayer.setPosition(evt.coordinate)
    //     }


    
        // {
        //     layerFilter: function(layer){
        //         return layer.get('title') === "StamenTerrain"
        //     }
        // }





    //     )
    // })





}






